package com.arkrev.grpcse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arkrev.grpcse.grpc.TimeSeries;
import com.arkrev.grpcse.manager.EnergyManager;
import com.arkrev.grpcse.service.EnergyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnergyServiceImpl implements EnergyService {

	@Autowired
	private EnergyManager energyManager;

	@Override
	public TimeSeries getElectricityConsumption(String time) {
		log.info("time is :" + time);
		return energyManager.getElectricityConsumption(time);
	}

	@Override
	public TimeSeries getElectricityConsumption() {
		return energyManager.getElectricityConsumption();
	}

}
