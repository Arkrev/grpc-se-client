package com.arkrev.grpcse.service;

import javax.validation.constraints.NotBlank;

import org.springframework.validation.annotation.Validated;

import com.arkrev.grpcse.grpc.TimeSeries;

@Validated
public interface EnergyService {
	public TimeSeries getElectricityConsumption(@NotBlank String time);

	TimeSeries getElectricityConsumption();
}
