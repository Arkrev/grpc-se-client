package com.arkrev.grpcse.api;

public interface ApiRegistration {

	public static final String MONITORING = "/monitoring";
	public static final String ELECTRICITY = "/electricity";
	public static final String CONSUMPTION = "/consumption";
	public static final String TIME = "/time";

	// template
	public static final String TEMPLATE_TIME = "/{time}";

}
