package com.arkrev.grpcse.manager.impl;

import org.springframework.stereotype.Component;

import com.arkrev.grpcse.grpc.ElectricityConsumptionRequest;
import com.arkrev.grpcse.grpc.ElectricityConsumptionResponse;
import com.arkrev.grpcse.grpc.EmptyRequest;
import com.arkrev.grpcse.grpc.MonitoringGrpc;
import com.arkrev.grpcse.grpc.MonitoringGrpc.MonitoringBlockingStub;
import com.arkrev.grpcse.grpc.TimeSeries;
import com.arkrev.grpcse.manager.EnergyManager;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EnergyManagerImpl implements EnergyManager {

	private MonitoringBlockingStub monitoringStub;

	public EnergyManagerImpl() {
		ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
		monitoringStub = MonitoringGrpc.newBlockingStub(managedChannel);
	}

	@Override
	public TimeSeries getElectricityConsumption(String time) {

		// init request
		ElectricityConsumptionRequest.Builder electricityConsumptionRequestBuilder = ElectricityConsumptionRequest
				.newBuilder();
		electricityConsumptionRequestBuilder.setTime(time);
		log.info("server request is : " + electricityConsumptionRequestBuilder.toString());

		// call server
		ElectricityConsumptionResponse electricityConsumption = monitoringStub
				.getElectricityConsumptionAtTime(electricityConsumptionRequestBuilder.build());
		log.info("server response is : " + electricityConsumption.toString());
		// extract response

		return electricityConsumption.getTimeSeries();
	}

	@Override
	public TimeSeries getElectricityConsumption() {

		// call server
		ElectricityConsumptionResponse electricityConsumption = monitoringStub
				.getElectricityConsumption(EmptyRequest.newBuilder().build());
		log.info("server response is : " + electricityConsumption.toString());
		// extract response

		return electricityConsumption.getTimeSeries();
	}

}
