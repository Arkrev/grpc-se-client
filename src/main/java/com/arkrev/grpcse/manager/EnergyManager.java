package com.arkrev.grpcse.manager;

import com.arkrev.grpcse.grpc.TimeSeries;

public interface EnergyManager {
	public TimeSeries getElectricityConsumption(String time);

	TimeSeries getElectricityConsumption();
}
