package com.arkrev.grpcse.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arkrev.grpcse.api.ApiRegistration;
import com.arkrev.grpcse.service.EnergyService;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

@RestController
@RequestMapping(ApiRegistration.MONITORING)
public class MonitoringWS {

	@Autowired
	private EnergyService energyService;

	@GetMapping(value = ApiRegistration.ELECTRICITY + ApiRegistration.CONSUMPTION
			+ ApiRegistration.TIME, produces = "application/json")
	public String getElectricityConsumption(@RequestParam(name = "time") String time)
			throws InvalidProtocolBufferException {
		return JsonFormat.printer().print(energyService.getElectricityConsumption(time));

	}

	@GetMapping(value = ApiRegistration.ELECTRICITY + ApiRegistration.CONSUMPTION, produces = "application/json")
	public String getElectricityConsumption() throws InvalidProtocolBufferException {
		return JsonFormat.printer().print(energyService.getElectricityConsumption());

	}

}
