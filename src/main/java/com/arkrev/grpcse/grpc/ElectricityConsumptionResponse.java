// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: monitoring.proto

package com.arkrev.grpcse.grpc;

/**
 * Protobuf type {@code ElectricityConsumptionResponse}
 */
public  final class ElectricityConsumptionResponse extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:ElectricityConsumptionResponse)
    ElectricityConsumptionResponseOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ElectricityConsumptionResponse.newBuilder() to construct.
  private ElectricityConsumptionResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ElectricityConsumptionResponse() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private ElectricityConsumptionResponse(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            com.arkrev.grpcse.grpc.TimeSeries.Builder subBuilder = null;
            if (timeSeries_ != null) {
              subBuilder = timeSeries_.toBuilder();
            }
            timeSeries_ = input.readMessage(com.arkrev.grpcse.grpc.TimeSeries.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(timeSeries_);
              timeSeries_ = subBuilder.buildPartial();
            }

            break;
          }
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.arkrev.grpcse.grpc.MonitoringOuterClass.internal_static_ElectricityConsumptionResponse_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.arkrev.grpcse.grpc.MonitoringOuterClass.internal_static_ElectricityConsumptionResponse_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.class, com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.Builder.class);
  }

  public static final int TIMESERIES_FIELD_NUMBER = 1;
  private com.arkrev.grpcse.grpc.TimeSeries timeSeries_;
  /**
   * <code>.TimeSeries timeSeries = 1;</code>
   */
  public boolean hasTimeSeries() {
    return timeSeries_ != null;
  }
  /**
   * <code>.TimeSeries timeSeries = 1;</code>
   */
  public com.arkrev.grpcse.grpc.TimeSeries getTimeSeries() {
    return timeSeries_ == null ? com.arkrev.grpcse.grpc.TimeSeries.getDefaultInstance() : timeSeries_;
  }
  /**
   * <code>.TimeSeries timeSeries = 1;</code>
   */
  public com.arkrev.grpcse.grpc.TimeSeriesOrBuilder getTimeSeriesOrBuilder() {
    return getTimeSeries();
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (timeSeries_ != null) {
      output.writeMessage(1, getTimeSeries());
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (timeSeries_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getTimeSeries());
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.arkrev.grpcse.grpc.ElectricityConsumptionResponse)) {
      return super.equals(obj);
    }
    com.arkrev.grpcse.grpc.ElectricityConsumptionResponse other = (com.arkrev.grpcse.grpc.ElectricityConsumptionResponse) obj;

    boolean result = true;
    result = result && (hasTimeSeries() == other.hasTimeSeries());
    if (hasTimeSeries()) {
      result = result && getTimeSeries()
          .equals(other.getTimeSeries());
    }
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasTimeSeries()) {
      hash = (37 * hash) + TIMESERIES_FIELD_NUMBER;
      hash = (53 * hash) + getTimeSeries().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.arkrev.grpcse.grpc.ElectricityConsumptionResponse prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code ElectricityConsumptionResponse}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:ElectricityConsumptionResponse)
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponseOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.arkrev.grpcse.grpc.MonitoringOuterClass.internal_static_ElectricityConsumptionResponse_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.arkrev.grpcse.grpc.MonitoringOuterClass.internal_static_ElectricityConsumptionResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.class, com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.Builder.class);
    }

    // Construct using com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      if (timeSeriesBuilder_ == null) {
        timeSeries_ = null;
      } else {
        timeSeries_ = null;
        timeSeriesBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.arkrev.grpcse.grpc.MonitoringOuterClass.internal_static_ElectricityConsumptionResponse_descriptor;
    }

    @java.lang.Override
    public com.arkrev.grpcse.grpc.ElectricityConsumptionResponse getDefaultInstanceForType() {
      return com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.getDefaultInstance();
    }

    @java.lang.Override
    public com.arkrev.grpcse.grpc.ElectricityConsumptionResponse build() {
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public com.arkrev.grpcse.grpc.ElectricityConsumptionResponse buildPartial() {
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse result = new com.arkrev.grpcse.grpc.ElectricityConsumptionResponse(this);
      if (timeSeriesBuilder_ == null) {
        result.timeSeries_ = timeSeries_;
      } else {
        result.timeSeries_ = timeSeriesBuilder_.build();
      }
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return (Builder) super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.arkrev.grpcse.grpc.ElectricityConsumptionResponse) {
        return mergeFrom((com.arkrev.grpcse.grpc.ElectricityConsumptionResponse)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.arkrev.grpcse.grpc.ElectricityConsumptionResponse other) {
      if (other == com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.getDefaultInstance()) return this;
      if (other.hasTimeSeries()) {
        mergeTimeSeries(other.getTimeSeries());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.arkrev.grpcse.grpc.ElectricityConsumptionResponse) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private com.arkrev.grpcse.grpc.TimeSeries timeSeries_ = null;
    private com.google.protobuf.SingleFieldBuilderV3<
        com.arkrev.grpcse.grpc.TimeSeries, com.arkrev.grpcse.grpc.TimeSeries.Builder, com.arkrev.grpcse.grpc.TimeSeriesOrBuilder> timeSeriesBuilder_;
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public boolean hasTimeSeries() {
      return timeSeriesBuilder_ != null || timeSeries_ != null;
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public com.arkrev.grpcse.grpc.TimeSeries getTimeSeries() {
      if (timeSeriesBuilder_ == null) {
        return timeSeries_ == null ? com.arkrev.grpcse.grpc.TimeSeries.getDefaultInstance() : timeSeries_;
      } else {
        return timeSeriesBuilder_.getMessage();
      }
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public Builder setTimeSeries(com.arkrev.grpcse.grpc.TimeSeries value) {
      if (timeSeriesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        timeSeries_ = value;
        onChanged();
      } else {
        timeSeriesBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public Builder setTimeSeries(
        com.arkrev.grpcse.grpc.TimeSeries.Builder builderForValue) {
      if (timeSeriesBuilder_ == null) {
        timeSeries_ = builderForValue.build();
        onChanged();
      } else {
        timeSeriesBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public Builder mergeTimeSeries(com.arkrev.grpcse.grpc.TimeSeries value) {
      if (timeSeriesBuilder_ == null) {
        if (timeSeries_ != null) {
          timeSeries_ =
            com.arkrev.grpcse.grpc.TimeSeries.newBuilder(timeSeries_).mergeFrom(value).buildPartial();
        } else {
          timeSeries_ = value;
        }
        onChanged();
      } else {
        timeSeriesBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public Builder clearTimeSeries() {
      if (timeSeriesBuilder_ == null) {
        timeSeries_ = null;
        onChanged();
      } else {
        timeSeries_ = null;
        timeSeriesBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public com.arkrev.grpcse.grpc.TimeSeries.Builder getTimeSeriesBuilder() {
      
      onChanged();
      return getTimeSeriesFieldBuilder().getBuilder();
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    public com.arkrev.grpcse.grpc.TimeSeriesOrBuilder getTimeSeriesOrBuilder() {
      if (timeSeriesBuilder_ != null) {
        return timeSeriesBuilder_.getMessageOrBuilder();
      } else {
        return timeSeries_ == null ?
            com.arkrev.grpcse.grpc.TimeSeries.getDefaultInstance() : timeSeries_;
      }
    }
    /**
     * <code>.TimeSeries timeSeries = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        com.arkrev.grpcse.grpc.TimeSeries, com.arkrev.grpcse.grpc.TimeSeries.Builder, com.arkrev.grpcse.grpc.TimeSeriesOrBuilder> 
        getTimeSeriesFieldBuilder() {
      if (timeSeriesBuilder_ == null) {
        timeSeriesBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            com.arkrev.grpcse.grpc.TimeSeries, com.arkrev.grpcse.grpc.TimeSeries.Builder, com.arkrev.grpcse.grpc.TimeSeriesOrBuilder>(
                getTimeSeries(),
                getParentForChildren(),
                isClean());
        timeSeries_ = null;
      }
      return timeSeriesBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:ElectricityConsumptionResponse)
  }

  // @@protoc_insertion_point(class_scope:ElectricityConsumptionResponse)
  private static final com.arkrev.grpcse.grpc.ElectricityConsumptionResponse DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.arkrev.grpcse.grpc.ElectricityConsumptionResponse();
  }

  public static com.arkrev.grpcse.grpc.ElectricityConsumptionResponse getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ElectricityConsumptionResponse>
      PARSER = new com.google.protobuf.AbstractParser<ElectricityConsumptionResponse>() {
    @java.lang.Override
    public ElectricityConsumptionResponse parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new ElectricityConsumptionResponse(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<ElectricityConsumptionResponse> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ElectricityConsumptionResponse> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public com.arkrev.grpcse.grpc.ElectricityConsumptionResponse getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

