package com.arkrev.grpcse.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: monitoring.proto")
public final class MonitoringGrpc {

  private MonitoringGrpc() {}

  public static final String SERVICE_NAME = "Monitoring";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.arkrev.grpcse.grpc.ElectricityConsumptionRequest,
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getGetElectricityConsumptionAtTimeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetElectricityConsumptionAtTime",
      requestType = com.arkrev.grpcse.grpc.ElectricityConsumptionRequest.class,
      responseType = com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.arkrev.grpcse.grpc.ElectricityConsumptionRequest,
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getGetElectricityConsumptionAtTimeMethod() {
    io.grpc.MethodDescriptor<com.arkrev.grpcse.grpc.ElectricityConsumptionRequest, com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getGetElectricityConsumptionAtTimeMethod;
    if ((getGetElectricityConsumptionAtTimeMethod = MonitoringGrpc.getGetElectricityConsumptionAtTimeMethod) == null) {
      synchronized (MonitoringGrpc.class) {
        if ((getGetElectricityConsumptionAtTimeMethod = MonitoringGrpc.getGetElectricityConsumptionAtTimeMethod) == null) {
          MonitoringGrpc.getGetElectricityConsumptionAtTimeMethod = getGetElectricityConsumptionAtTimeMethod = 
              io.grpc.MethodDescriptor.<com.arkrev.grpcse.grpc.ElectricityConsumptionRequest, com.arkrev.grpcse.grpc.ElectricityConsumptionResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Monitoring", "GetElectricityConsumptionAtTime"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.arkrev.grpcse.grpc.ElectricityConsumptionRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MonitoringMethodDescriptorSupplier("GetElectricityConsumptionAtTime"))
                  .build();
          }
        }
     }
     return getGetElectricityConsumptionAtTimeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.arkrev.grpcse.grpc.EmptyRequest,
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getGetElectricityConsumptionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetElectricityConsumption",
      requestType = com.arkrev.grpcse.grpc.EmptyRequest.class,
      responseType = com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.arkrev.grpcse.grpc.EmptyRequest,
      com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getGetElectricityConsumptionMethod() {
    io.grpc.MethodDescriptor<com.arkrev.grpcse.grpc.EmptyRequest, com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getGetElectricityConsumptionMethod;
    if ((getGetElectricityConsumptionMethod = MonitoringGrpc.getGetElectricityConsumptionMethod) == null) {
      synchronized (MonitoringGrpc.class) {
        if ((getGetElectricityConsumptionMethod = MonitoringGrpc.getGetElectricityConsumptionMethod) == null) {
          MonitoringGrpc.getGetElectricityConsumptionMethod = getGetElectricityConsumptionMethod = 
              io.grpc.MethodDescriptor.<com.arkrev.grpcse.grpc.EmptyRequest, com.arkrev.grpcse.grpc.ElectricityConsumptionResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Monitoring", "GetElectricityConsumption"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.arkrev.grpcse.grpc.EmptyRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.arkrev.grpcse.grpc.ElectricityConsumptionResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MonitoringMethodDescriptorSupplier("GetElectricityConsumption"))
                  .build();
          }
        }
     }
     return getGetElectricityConsumptionMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MonitoringStub newStub(io.grpc.Channel channel) {
    return new MonitoringStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MonitoringBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MonitoringBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MonitoringFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MonitoringFutureStub(channel);
  }

  /**
   */
  public static abstract class MonitoringImplBase implements io.grpc.BindableService {

    /**
     */
    public void getElectricityConsumptionAtTime(com.arkrev.grpcse.grpc.ElectricityConsumptionRequest request,
        io.grpc.stub.StreamObserver<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetElectricityConsumptionAtTimeMethod(), responseObserver);
    }

    /**
     */
    public void getElectricityConsumption(com.arkrev.grpcse.grpc.EmptyRequest request,
        io.grpc.stub.StreamObserver<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetElectricityConsumptionMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetElectricityConsumptionAtTimeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.arkrev.grpcse.grpc.ElectricityConsumptionRequest,
                com.arkrev.grpcse.grpc.ElectricityConsumptionResponse>(
                  this, METHODID_GET_ELECTRICITY_CONSUMPTION_AT_TIME)))
          .addMethod(
            getGetElectricityConsumptionMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.arkrev.grpcse.grpc.EmptyRequest,
                com.arkrev.grpcse.grpc.ElectricityConsumptionResponse>(
                  this, METHODID_GET_ELECTRICITY_CONSUMPTION)))
          .build();
    }
  }

  /**
   */
  public static final class MonitoringStub extends io.grpc.stub.AbstractStub<MonitoringStub> {
    private MonitoringStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MonitoringStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MonitoringStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MonitoringStub(channel, callOptions);
    }

    /**
     */
    public void getElectricityConsumptionAtTime(com.arkrev.grpcse.grpc.ElectricityConsumptionRequest request,
        io.grpc.stub.StreamObserver<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetElectricityConsumptionAtTimeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getElectricityConsumption(com.arkrev.grpcse.grpc.EmptyRequest request,
        io.grpc.stub.StreamObserver<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetElectricityConsumptionMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MonitoringBlockingStub extends io.grpc.stub.AbstractStub<MonitoringBlockingStub> {
    private MonitoringBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MonitoringBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MonitoringBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MonitoringBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.arkrev.grpcse.grpc.ElectricityConsumptionResponse getElectricityConsumptionAtTime(com.arkrev.grpcse.grpc.ElectricityConsumptionRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetElectricityConsumptionAtTimeMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.arkrev.grpcse.grpc.ElectricityConsumptionResponse getElectricityConsumption(com.arkrev.grpcse.grpc.EmptyRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetElectricityConsumptionMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MonitoringFutureStub extends io.grpc.stub.AbstractStub<MonitoringFutureStub> {
    private MonitoringFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MonitoringFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MonitoringFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MonitoringFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getElectricityConsumptionAtTime(
        com.arkrev.grpcse.grpc.ElectricityConsumptionRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetElectricityConsumptionAtTimeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse> getElectricityConsumption(
        com.arkrev.grpcse.grpc.EmptyRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetElectricityConsumptionMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ELECTRICITY_CONSUMPTION_AT_TIME = 0;
  private static final int METHODID_GET_ELECTRICITY_CONSUMPTION = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MonitoringImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MonitoringImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ELECTRICITY_CONSUMPTION_AT_TIME:
          serviceImpl.getElectricityConsumptionAtTime((com.arkrev.grpcse.grpc.ElectricityConsumptionRequest) request,
              (io.grpc.stub.StreamObserver<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse>) responseObserver);
          break;
        case METHODID_GET_ELECTRICITY_CONSUMPTION:
          serviceImpl.getElectricityConsumption((com.arkrev.grpcse.grpc.EmptyRequest) request,
              (io.grpc.stub.StreamObserver<com.arkrev.grpcse.grpc.ElectricityConsumptionResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MonitoringBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MonitoringBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.arkrev.grpcse.grpc.MonitoringOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Monitoring");
    }
  }

  private static final class MonitoringFileDescriptorSupplier
      extends MonitoringBaseDescriptorSupplier {
    MonitoringFileDescriptorSupplier() {}
  }

  private static final class MonitoringMethodDescriptorSupplier
      extends MonitoringBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MonitoringMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MonitoringGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MonitoringFileDescriptorSupplier())
              .addMethod(getGetElectricityConsumptionAtTimeMethod())
              .addMethod(getGetElectricityConsumptionMethod())
              .build();
        }
      }
    }
    return result;
  }
}
